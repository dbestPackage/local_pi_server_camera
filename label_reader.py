#label_reader.py
#Created by: Dale Best
#April 13th 2018


import cv2
import ftplib
import datetime
import threading
from io import BytesIO
import queue 
from WebcamVideoStream import WebcamVideoStream


CAMERA_SOURCE_ID = 1 #0 is typically the attached webcam
CAMERA_PIXEL_HEIGHT = 720
CAMERA_PIXEL_WIDTH = 1280
CAMERA_FPS = 60

FTP_SERVER_ADDRESS = '10.0.0.2'
FTP_SERVER_USER = 'pi'
FTP_SERVER_PASSWORD = 'P@ck@g3s'

CAMERA_FRAME_QUEUE = queue.Queue()

session_running = True
cv2.namedWindow('Scanning Session', cv2.WINDOW_NORMAL)
cv2.resizeWindow('Scanning Session', CAMERA_PIXEL_WIDTH,CAMERA_PIXEL_HEIGHT)

#Spin up a ftp session
session = ftplib.FTP(FTP_SERVER_ADDRESS,FTP_SERVER_USER,FTP_SERVER_PASSWORD)
session.cwd('files/images')

def upload_frame(frame,ftp_session):
  status, img_data = cv2.imencode('.jpg',frame)
  img_file = BytesIO()
  img_string = img_data.tostring()
  img_file.write(img_string)

  img_file.seek(0)

  PHOTO_TIME = datetime.datetime.now().strftime("%Y-%m-%dB-B%H-%M-%S")
  PHOTO_NAME = PHOTO_TIME + '.jpg'
  FTP_FILE_NAME = 'STOR ' + PHOTO_NAME
  
  ftp_session.storbinary(FTP_FILE_NAME, img_file)

  img_file.close()
  return

vs = WebcamVideoStream(src=1).start()


while (session_running):
  camera_frame = vs.read()
  cv2.imshow('Scanning Session',camera_frame)
  input_key = cv2.waitKey(1)

  if input_key & 0xFF == ord('q'):
    session_running = False
  elif input_key & 0xFF == ord('s'):
    upload_frame(img,session)
    #TODO: Try to clear out built up buffer

session.quit()